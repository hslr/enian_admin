<?php
    // enianAdmin 后端PHP配置信息文件
	return [

		//无需登录验证就可进入的执行的方法（跳转的页面）仅针对admin模块下的index控制器
		'jump_login_check_names'=>	['login','verify'],
		
		
		//验证码配置（仅针对admin模块登录）
		'captcha'  => [
                // 验证码字符集合
                'codeSet'  => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY', 
                // 验证码字体大小(px)
                'fontSize' => 30, 
                // 是否画混淆曲线 -建议开启
                'useCurve' => true, 
                // 验证码位数
                'length'   => 4, 
                // 验证成功后是否重置        
                'reset'    => true
        ],

        // enianAdmin 框架信息
        'enianAdmin' =>[
            //后台网站LOGO标题
            'logo_title' =>'enianAdmin',
            
            //以下不可修改
            'v'     => '0.1.5.190607',
            'v_num' => 1
        ] 
	];
?>