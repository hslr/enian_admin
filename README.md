# enianAdmin 0.1.5版本（此版本已旧，建议使用新版enianAdmin 1.0 [点此了解详情](https://gitee.com/hslr/enian_admin/tree/v1/)。）
##### 0.1.5.190607 做了很大的更新，更新日志：https://enianteam.com/enian_admin/index/update_log.html
> 注意：此版本已旧，建议使用新版enianAdmin 1.0 [点此了解详情](https://gitee.com/hslr/enian_admin/tree/v1/)。

#### QQ群：372920173 验证信息：enian 。我是一个PHP后端开发，欢迎加入群我们共同学习。

## 文档/体验（体验版为最新版本，并非v0.1.5）
- 详细文档：http://doc.enianteam.com/enian_admin.html
- 后台体验地址：http://dadmin.enianteam.com/
- 前台体验地址：http://demo.enianteam.com/

## 第三方依赖
- thinkPHP 5.0.24
- layui 2.4.5

## 一.安装教程
#### 1 文件部署
将文件下载后，复制到服务器网站路径下，并将网站首页目录指向`/public`下

#### 2.1 数据库导入，方法1（推荐）
1. 浏览器打开：http://您的域名/install 按步骤来即可

#### 2.2 数据库导入，方法2
1. 将`enian_admin.sql`文件导入数据库中
2. 手动删除`/public/install`（安装）目录
3. 自行修改数据库配置信息文件`/application/database.php`


#### 3 修改路由文件（`/application/route.php`）
为了让域名网址更短，统一，同时支持搭建多个域名统一管理

1.部署前请到配置文件（`/application/config.php`）改：

```php
 'url_domain_deploy' =>  true	//如果是false改为true
```

2.然后到`/application/route.php`更改路由文件内容
```php
return [
    '__domain__'=>[
        'admin'     => 	'admin',//用admin二级域名 访问admin（enianAdmin核心）模块，
        //可以直接使用ip地址或完整域名绑定到admin模块
        //'127.0.0.1' =>	'admin',
    ],
    // 下面是路由规则定义
];
```
> 以上内容可以到 tp5参考文档：https://www.kancloud.cn/manual/thinkphp5/118039


## 二.目录结构

#### 前端目录结构
```
public/static/admin						enianAdmin前端核心目录
├── core 								核心文件夹（内容可增加不可删除）
│   ├── default.js 						默认首页js 文件
│   └── iframe.js 						iframe 核心js文件
├── cssTheme 							css主题文件（内容可增加可修改）
│   ├── example.css
│   ├── fresh_1.css
│   ├── fresh.css
│   ├── warm.css
│   └── wind_red.css
├── errTpl								错误页模板
│   ├── 404.html
│   ├── accident.html
│   ├── empty.html
│   └── error.html
├── s_app								sapp核心目录（不可修改）
├── src 								静态资源文件目录（可修改增加）可以储存模块或其他任意资源文件
│   ├── login							admin模块登录页的静态文件
│   │   ├── css
│   │   ├── img
│   │   └── js
│   ├── editormd
│   └── ueditor
├── config.js 							配置文件，可增加修改
├── enianAdmin.js 						核心js文件，不可修改
│
```

#### 后端目录结构 （包括前端模板）

```
application 								应用目录 -tp5
├── admin									admin模块（enianAdmin主模块）
│   ├── common.php 							模块公共文件-tp5
│   ├── config.php 							模块配置文件-tp5|主要配置文件-框架
│   ├── controller 							控制器目录
│   │   ├── Common.php 						核心公共控制器[不可修改]
│   │   ├── Database.php 					数据库模板控制器 -暂未使用
│   │   ├── Index.php 						框架核心控制器[不可修改]
│   │   ├── File.php 						[扩展] 文件管理控制器
│   │   └── ...								其他控制器文件
│   ├── model								模型-tp5
│   │   └── Getlognamedata.php 				
│   └── view 								视图模板目录
│       ├── file 							文件管理 模板目录
│       │   └── uploadFileManager.html 		
│       ├── index							框架核心 模板目录
│       │   ├── about.html 					关于页面模板文件
│       │   ├── addAdmin.html 				增加管理员（用户）模板
│       │   ├── addApp.html 				增加应用项目模板
│       │   ├── addSonApp.html 				增加子应用模板
│       │   ├── adminList.html 				管理员（用户）列表模板
│       │   ├── applist.html 				应用项目模板
│       │   ├── login.html 					登录模板
│       │   ├── myAppList.html 				我的应用模板
│       │   ├── o_setMenuLeft.html 			左侧菜单修改-开发版 模板
│       │   ├── setAdminAuthGroup.html 		设置用户组权限模板
│       │   ├── setAdminAuth.html 			设置用户权限
│       │   ├── setMenuLeft.html 			设置左侧菜单模板
│       │   ├── setting.html 				个性设置模板
│       │   ├── setUserGroup.html 			设置用户组权限模板
│       │   ├── sonApplist.html 			子应用列表模板
│       │   ├── sysLogList.html 			系统日志模板
│       │   ├── test.html 					测试页 模板
│       │   ├── updatePassword.html 		修改密码 模板
│       │   └── userGroupList.html 			用户组列表 模板
│       └── layout 							布局文件目录[不可修改-可新增]
│           ├── empty.html
│           ├── home.html
│           └── page.html
├── ... 									其他模块及配置文件，建议去tp5.0官方文档 - 目录结构查看
```