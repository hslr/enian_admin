<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request; 
class Common extends Controller{
	
	function _initialize(){
		$this->init();
	}
	
	function init(){
		if(!$this->isJumpLoginCheck()){
			if(!session('loginkey')){
				$this->redirect(url('index/login'));
			}
			$loginkey=session('loginkey');
			$userid=session('userid');
			//dump($userid);
			$cache_name='admin_key_aid_'.$userid;
		 	$db_loginkey=Db::name('admin')->cache($cache_name,0,'admin_key')->where('aid',$userid)->value('loginkey');
			if(!$userid || $db_loginkey!=$loginkey){
				$this->error('您的登录已过期.... 请重新登录！',url('index/login'));
			}
		}
		$this->is_has_auth();
		
	}
	private function isJumpLoginCheck(){
		//开发前请先配置一下 	'jump_login_check_names'=>	['login'],   
		if(is_array(config('jump_login_check_names')) && in_array(request()->action(), config('jump_login_check_names'))){
			return true;
		}else{
			return false;
		}
	}
	
	
	private function is_has_auth(){
		//查看本页地址是否在权限列表中，不在 过
		//如果在判断管理员是否存在权限。存在 过。不存在 拒绝
		$paths = request()->dispatch();
		$path = $paths['module'][1].'/'.$paths['module'][2];
		$aWhere['is_auth'] = 1;
		$aWhere['url'] =$path;
		$is_find = Db::name('son_apps')->where($aWhere)->find();
		if($is_find){
			//查找当前管理员的权限
			$where['aid']=session('userid');
			if($where['aid']==1){
				return true;
			}
			$getAdminData = Db::name('admin')->where($where)->find();
			//权限分组所有的权限
			$group = $getAdminData['group'];
			if($group){
				$group = json_decode($group,true);
				foreach ($group as $k => $v) {
					$groupWhere['id']=$v;
					//已启用
					$groupWhere['status']=1;
					$groupAuth= Db::name('user_group')->where($groupWhere)->value('auth');
					if($groupAuth){
						$groupAuth = json_decode($groupAuth,true);
						foreach ($groupAuth as $key => $value) {
							$allAuth[] = $value;
						}
					}
					
				}
				//停止使用单独设置的权限，全部使用用户组
				if(!$allAuth || !in_array($is_find['sid'], $allAuth)){
					//不存在
					$this->error('没有权限，拒绝访问',url('index/index'));
				}
			}else{
				$this->error('没有权限，拒绝访问',url('index/index'));
			}
		}
	}

	public function isLogin(){
		//空操作...
	}
	
	
	public function error_(){
		return json(array('code'=>101,'msg'=>"没有权限，拒绝访问"));
	}
	
	/*
	 * 获取用户信息
	 * @param uid 	[可空]用户id，留空获取当前管理员信息
	 */
	public function getUserInfo($uid=null){
		if(!$uid){
			$uid = session('userid');
		}
		$userInfo = Db::name('admin')->where('aid',$uid)->find();
		if($userInfo){
			$userGroup = json_encode($userInfo['group']);
			$userGroup = is_array($userGroup)?$userGroup:array();
			$where='';
			foreach ($userGroup as $key => $val) {
				$where['id']= $val;
			}
			$userGroup = Db::name('user_group')->whereOr($where)->field('id,name')->select();
			foreach ($userGroup as $key => $val) {
				$group[$val['id']] = $val['name'];
			}
			$userInfo['group'] = $group;
			return $userInfo;
		}
		return false;
	}
	
	//核心操作，页面加载必执行方法
	public function loadHomeTpl($name){
		switch (input('param.PAGE_TYPE')) {
			case 1:
				//ajax请求的页面			
				$this -> view -> engine -> layout('layout/empty');
				
				if(method_exists($this, $name)){
					return $name;	
				}else{
					return json(array('code'=>404,'msg'=>'404页面不存在'));
				}
				break;
			case 2:
				//独立页的页面带模板page的
				$this -> view -> engine -> layout('layout/page');
				return $name;
				break;
			
			default:
				//执行默认模板显示
				
				//加载用户的菜单数据
				$where['aid'] = session('userid');
				$where['type'] = 0;
				$data = Db::name('menu') -> where($where) -> order('order asc id asc') ->field('* , note as pid') -> select();
				if (!$data) {
					$data = Db::name('menu') -> where('type', 1) -> order('order asc') ->field('* , note as pid') -> select();
				}
				foreach ($data as $k => $v) {
					$json = json_decode($v['note2'], true);
					$data[$k]['type'] = $json['type'];
					unset($data[$k]['note'], $data[$k]['note2']);
				}
				$data = json_encode($data);
				$adminName = Db::name('admin') -> where('aid', session('userid')) -> value('name');
				$returnData = array('name' => $adminName, 'homeurl' => '', 'data' => $data);
				//此处写在框架模板页面和要渲染的数据
				return view('layout/home', $returnData);
				break;
		}
	}
	

}
