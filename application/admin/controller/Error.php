<?php
namespace app\admin\controller;

//控制器空操作
class Error  extends Common {
	public function _empty($name) {
		//以下代码必须建在每个控制器的_empty的操作中
		$func = $this -> loadHomeTpl($name);
		if (is_object($func)) {
			return $func;
		} else {
			return $this -> $func();
		}
	}

}
?>