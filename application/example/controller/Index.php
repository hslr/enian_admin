<?php
namespace app\example\controller;
use think\Controller;
use think\Db;

class Index extends Controller{
	
	public function index() {
		$return_data['config'] = Db::name('app_example_config')->column('value','name');
		$return_data['ent'] = Db::name('app_example_content')->where('type',0)->select();
		$return_data['content'] = Db::name('app_example_content')->where('type',1)->select();

		return view('',$return_data);
	}

}
