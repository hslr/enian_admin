/*
 * Name:	enianAdmin 前端配置文件
 * Author:	红烧猎人
 */

enianAdmin.config = function(config){
	//配置文件目录
	//根目录
	var root = enianAdmin.rootPath;
	var allConfig = {
		//目录配置
		path:{
			core:root+"core"	//核心框架js文件
			,theme:root+"cssTheme"	//主题目录配置
			,errtpl:root+"errTpl"	//错误模板页面
		},
		//主题列表
		themeList:{
			fresh_1:'清爽+'
			,fresh:'清爽'
			,warm:'暖色'
			,wind_red:'酒红+'
			,test:'测试'
		}
		//版本缓存号（用于更新layui等）可以设定 cacheVersion:new Date().getTime(),禁止缓存
		,cacheVersion:'0.3'
		
	}
	if(allConfig[config]){
		return allConfig[config];
	}else{
		return null;
	}
}
