<?php
namespace app\admin\model;

class Getlognamedata{
	
	//获得日志内容类型的名字
	public  function getLogTypeContent($type,$log_content_type){
		$data = [
		//用户管理
			2 => [
				1 => '用户添加',
				2 => '用户删除',
				3 => '用户修改'
			],
			
		//应用管理
			4 => [
				1 => '应用添加',
				2 => '应用删除',
				3 => '应用修改',
				4 => '子应用添加',
				5 => '子应用删除',
				6 => '子应用修改'
			]
		
		
		
		
		];
		if($log_content_type){
			return $data[$type][$log_content_type];
		}else{
			return '';
		}
		
	}
	
	//获取内容字段的其他数据
	public  function getLogContentKey($type,$log_content_type){
		//dump($type);
		$data = [
		//用户登录
			1 => [
				'username' => '用户名',
				'password' => '密码',
			],
		//用户管理
			2 => [
				'aid'	 => '用户id',
				'a_id'	 => '用户id',
				'username' => '用户名',
				'name' => '名字',
				'note' => '备注',
				'status' => '状态',
				'password' => '<div style="color:red;">密码</div>',
				'password' => '密码',
			],
			
		//应用管理
			4 => [
				'appid'	 => '应用id',
				'sid' => '子应用id',
				'name' => '名字',
				'note' => '备注',
				'icon' => '图标',
				'url' => '页面地址',
				'is_display' => '展示在应用页',
				'is_suth' => '权限',
				'type' => '类型',
				'order' => '排序',
			]
		];
		if(isset($data[$type]) && isset($data[$type][$log_content_type])){
			return $data[$type][$log_content_type];
		}else{
			return $log_content_type;
		}
		
	}
	
	//获取内容字段的其他数据和值
	public  function getLogContent($type,$key,$value){
		switch ($type) {
			case 2:
				//用户管理
				if($key == 'status'){
					return $value==0?'停用':'启用';
				}
				
				break;
			case 4:
				//用户管理
				if($key == 'is_display'){
					return $value==0?'否':'是';
				}
				if($key == 'is_suth'){
					return $value==0?'不加入权限':'加入权限';
				}
				
				break;
			default:
				
				break;
		}
		return $value;
	}
	
	public function getContent($type,$content){
		
		$contentArr = json_decode($content,true);
		//dump($type);
		$sonTypeId = isset($contentArr['log_content_type'])?$contentArr['log_content_type']:'';
		$sontype=$this ->getLogTypeContent($type,$sonTypeId);
		unset($contentArr['log_content_type']);
		$returnContent='';
		foreach ($contentArr as $k => $v) {
			$returnContent .= $this ->getLogContentKey($type,$k).' -> '.$this ->getLogContent($type,$k,$v) .' | ';
			
		}
		$sontype = $sontype!=""?'[ '.$sontype.' ]  ':'';
		return $sontype.$returnContent;
		
	}
}

?>