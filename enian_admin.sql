/*
 Navicat Premium Data Transfer

 Source Server         : 本地测试数据库
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : localhost:3306
 Source Schema         : enian_admin

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 07/06/2019 13:46:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for enian_admin
-- ----------------------------
DROP TABLE IF EXISTS `enian_admin`;
CREATE TABLE `enian_admin`  (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `menu` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` tinyint(3) DEFAULT NULL,
  `auth` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `start_time` bigint(20) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `last_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `last_time` bigint(20) DEFAULT NULL,
  `loginkey` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `group` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`aid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enian_admin
-- ----------------------------
INSERT INTO `enian_admin` VALUES (1, 'Administrator', '6c9cdce9f6d927cea4c621b33ca05013', '超级管理员', '主账号', NULL, 1, '', 1552665600, NULL, '127.0.0.1', 1559882096, 'c0ae2525c773cc766facbb0aa2c46560', '');

-- ----------------------------
-- Table structure for enian_app_example_config
-- ----------------------------
DROP TABLE IF EXISTS `enian_app_example_config`;
CREATE TABLE `enian_app_example_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '示例应用（example）配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enian_app_example_config
-- ----------------------------
INSERT INTO `enian_app_example_config` VALUES (1, 'title', '示例应用前台首页');
INSERT INTO `enian_app_example_config` VALUES (2, 'welcome_text', '欢迎使用enianAdmin，开发如此简单');

-- ----------------------------
-- Table structure for enian_app_example_content
-- ----------------------------
DROP TABLE IF EXISTS `enian_app_example_content`;
CREATE TABLE `enian_app_example_content`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '跳转地址',
  `type` int(1) DEFAULT NULL COMMENT '类型：0快捷入口，1内容列表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '示例应用（example）内容表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of enian_app_example_content
-- ----------------------------
INSERT INTO `enian_app_example_content` VALUES (1, 'enianAdmin官网首页', 'http://enianteam.com/enian_admin.html', 0);
INSERT INTO `enian_app_example_content` VALUES (2, 'enianAdmin开发文档', 'http://doc.enianteam.com/enian_admin.html', 0);
INSERT INTO `enian_app_example_content` VALUES (4, 'enianAdmin - 开发文档[0.1.x ]', 'http://doc.enianteam.com/enian_admin.html', 1);
INSERT INTO `enian_app_example_content` VALUES (5, '去码云为本项目点个Star，支持一下本项目', 'https://gitee.com/hslr/enian_admin', 1);
INSERT INTO `enian_app_example_content` VALUES (6, '去layui为本项目点个赞，支持一下本项目', 'https://fly.layui.com/case/u/9273432', 1);
INSERT INTO `enian_app_example_content` VALUES (7, 'layui官网文档', 'https://www.layui.com/doc/', 1);
INSERT INTO `enian_app_example_content` VALUES (8, 'thinkphp5.0官方文档', 'https://www.kancloud.cn/manual/thinkphp5/118003', 1);
INSERT INTO `enian_app_example_content` VALUES (9, '我的网站首页', 'http://enianteam.com', 1);
INSERT INTO `enian_app_example_content` VALUES (10, '我的博客首页', 'http://blog.enianteam.com/', 1);

-- ----------------------------
-- Table structure for enian_apps
-- ----------------------------
DROP TABLE IF EXISTS `enian_apps`;
CREATE TABLE `enian_apps`  (
  `appid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order` int(11) DEFAULT 99,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`appid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'App列表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enian_apps
-- ----------------------------
INSERT INTO `enian_apps` VALUES (1, '系统应用', 99, '所有系统自带的应用', NULL, NULL, NULL);
INSERT INTO `enian_apps` VALUES (21, '示例应用', 99, '访问地址：http://您的域名/example', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for enian_menu
-- ----------------------------
DROP TABLE IF EXISTS `enian_menu`;
CREATE TABLE `enian_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order` int(11) DEFAULT 99,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note2` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户菜单' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enian_menu
-- ----------------------------
INSERT INTO `enian_menu` VALUES (1, '常用', 1, 'layui-icon-rate-half', 1, 1, NULL, '', '0', NULL);
INSERT INTO `enian_menu` VALUES (2, '我的应用', 10, '', 1, 1, NULL, '/index/myapplist.html', '1', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (5, '文件管理', 99, '', 1, 1, NULL, '/file/uploadfilemanager.html', '1', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (10, '设置', 10, 'layui-icon-set', 1, 1, NULL, '', '0', NULL);
INSERT INTO `enian_menu` VALUES (11, '菜单设置（自定义）', 99, '', 1, 1, NULL, '/index/setmenuleft.html', '10', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (12, '个性设置', 99, '', 1, 1, NULL, '/index/setting.html', '10', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (20, '超级管理员', 20, 'layui-icon-username', 1, 1, NULL, '', '0', NULL);
INSERT INTO `enian_menu` VALUES (21, '系统用户', 21, '', 1, 1, NULL, '/index/adminlist.html', '20', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (22, '应用管理', 22, '', 1, 1, NULL, '/index/applist.html', '20', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (23, '系统日志', 99, '', 1, 1, NULL, '/index/sysloglist.html', '20', '{\"type\":\"0\"}');
INSERT INTO `enian_menu` VALUES (24, '用户组设置', 23, '', 1, 1, NULL, '/index/usergrouplist.html', '20', '{\"type\":\"0\"}');

-- ----------------------------
-- Table structure for enian_pic_lib
-- ----------------------------
DROP TABLE IF EXISTS `enian_pic_lib`;
CREATE TABLE `enian_pic_lib`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `src` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片库' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for enian_son_apps
-- ----------------------------
DROP TABLE IF EXISTS `enian_son_apps`;
CREATE TABLE `enian_son_apps`  (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `appid` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT 99,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `field` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `type` int(255) DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_display` int(255) DEFAULT NULL,
  `is_auth` int(255) DEFAULT NULL,
  PRIMARY KEY (`sid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '子应用列表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enian_son_apps
-- ----------------------------
INSERT INTO `enian_son_apps` VALUES (7, '设置管理权限', 1, 99, '设定管理员权限', NULL, NULL, NULL, NULL, 'index/setadminauth', 0, 1);
INSERT INTO `enian_son_apps` VALUES (8, '管理员列表', 1, 99, '系统所有管理员', NULL, NULL, NULL, NULL, 'index/adminList', 1, 1);
INSERT INTO `enian_son_apps` VALUES (9, '应用列表', 1, 99, '应用管理', NULL, NULL, NULL, NULL, 'index/applist', 1, 1);
INSERT INTO `enian_son_apps` VALUES (10, '子应用列表', 1, 99, '应用管理', NULL, NULL, NULL, NULL, 'index/sonapplist', 0, 1);
INSERT INTO `enian_son_apps` VALUES (11, '添加应用', 1, 99, '应用管理', NULL, NULL, NULL, NULL, 'index/addapp', 0, 1);
INSERT INTO `enian_son_apps` VALUES (12, '添加子应用', 1, 99, '应用管理', NULL, NULL, NULL, NULL, 'index/addSonApp', 0, 1);
INSERT INTO `enian_son_apps` VALUES (20, '文件管理', 1, 99, '上传/文件管理', NULL, NULL, NULL, NULL, 'file/uploadfilemanager', 1, 1);
INSERT INTO `enian_son_apps` VALUES (21, '系统日志', 1, 99, NULL, NULL, NULL, NULL, NULL, 'index/sysloglist', 1, 1);
INSERT INTO `enian_son_apps` VALUES (22, '用户组列表', 1, 99, NULL, NULL, NULL, NULL, NULL, 'index/usergrouplist', 1, 1);
INSERT INTO `enian_son_apps` VALUES (23, '用户组设置', 1, 99, NULL, NULL, NULL, NULL, NULL, 'index/setusergroup', 0, 1);
INSERT INTO `enian_son_apps` VALUES (24, '用户用户组修改', 1, 99, NULL, NULL, NULL, NULL, NULL, 'index/setadminauthgroup', 0, 1);
INSERT INTO `enian_son_apps` VALUES (33, '网站配置', 21, 99, '访问地址：http://您后台域名/example/config.html', NULL, NULL, NULL, 0, 'example/config', 1, 1);
INSERT INTO `enian_son_apps` VALUES (34, '快捷入口', 21, 99, '访问地址：http://您后台域名/example/ent.html', NULL, NULL, NULL, 0, 'example/ent', 1, 1);
INSERT INTO `enian_son_apps` VALUES (35, '内容管理', 21, 99, '访问地址：http://您后台域名/example/content.html', NULL, NULL, NULL, 0, 'example/content', 1, 1);
INSERT INTO `enian_son_apps` VALUES (36, '[查]快捷入口数据', 21, 99, '接口不需要展示在首页', NULL, NULL, NULL, 2, 'example/getentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (37, '[改]快捷入口数据', 21, 99, '只有加入权限开关打开的子应用才会在用户组设置权限中设置', NULL, NULL, NULL, 2, 'example/updateentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (38, '[增]快捷入口数据', 21, 99, '如果加入权限开关关闭，即代表所有用户都能访问该子应用', NULL, NULL, NULL, 2, 'example/addentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (39, '[删]快捷入口数据', 21, 99, '接口不需要展示在首页', NULL, NULL, NULL, 2, 'example/delentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (40, '[增]内容数据', 21, 99, '接口不需要展示在首页', NULL, NULL, NULL, 2, 'example/addcontentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (41, '[删]内容数据', 21, 99, '接口不需要展示在首页', NULL, NULL, NULL, 2, 'example/delcontentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (42, '[改]内容数据', 21, 99, '接口不需要展示在首页', NULL, NULL, NULL, 2, 'example/updatecontentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (43, '[查]内容数据', 21, 99, '接口不需要展示在首页', NULL, NULL, NULL, 2, 'example/getcontentdata', NULL, 1);
INSERT INTO `enian_son_apps` VALUES (44, 'enianAdmin使用手册', 21, 99, 'enianAdmin使用手册必读', NULL, NULL, NULL, 0, 'example/index', 1, NULL);

-- ----------------------------
-- Table structure for enian_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `enian_sys_log`;
CREATE TABLE `enian_sys_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for enian_user_group
-- ----------------------------
DROP TABLE IF EXISTS `enian_user_group`;
CREATE TABLE `enian_user_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int(11) DEFAULT 0 COMMENT '0,停用，1启用',
  `auth` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'json数据，权限列表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enian_user_group
-- ----------------------------
INSERT INTO `enian_user_group` VALUES (1, '超级管理', '', 0, '');

SET FOREIGN_KEY_CHECKS = 1;
