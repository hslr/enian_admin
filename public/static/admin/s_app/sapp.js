/*
 * 名称：不重新加载动跳转页面核心组件
 * 作者：红烧猎人
 * 版本：0.2
 * 日期：2019年1月15日
 */

(function() {

	var $ = $jq;
	
	var config ={
		elem : 'body'
		,errorConfig :{
			title:'页面加载失败'
			,html:'<h2 style="font-size:20px">页面加载失败了~</h2>'
		} 
		,sign:['PAGE_TYPE',1]
	}
	var sapp = function() {
		return new sapp.prototype.init();
	}
	sapp.$ = $;
	sapp.prototype = {
		//定义一些静态变量
 
		// 构造函数方法
		'init': function() {
			//定义一些变量属性
			//document.write("<script language=javascript src='/static/s_app/jquery.js'></script>");
		}
	}
	
	sapp.v = function(){
		return '0.2.2019-1-15 10:12:40';
	}
	
	//主动切换页面
	sapp.render = function(Obj) {
		var subpageObj = Obj.subpageObj,
			subpage = subpageObj.url || '',
			title = subpageObj.title || false,
			updateElem = Obj.elem||config.elem,
			updateUrlBar = Obj.updateUrlBar || false;
			errorConfig = Obj.errorConfig||false;
		var sign = false;
		if(typeof(Obj.sign)=="boolean"){
			if(Obj.sign){
				sign = config.sign;
			}
		}else{
			sign = Obj.sign || config.sign;
		}
		var newUrl = subpage;
		//如果设定标记则更改url
		var reg = /^(.*?)\?(.*?)$/;
		var url = String(subpage).match(reg);
		var get;
		if(url) {
			get = url[2];
			url = url[1];
		} else {
			url = subpage;
			get = '';
		}
		if(sign){
			newUrl = url + "?"+sign[0]+"="+sign[1]+"&" + get;
		}
		
		if(updateUrlBar) {
			url = (get == "") ? url + '' : url + '?' + get;
			history.pushState(subpageObj, '', url);
		}
		$.ajax({
			type: "get",
			url:newUrl,
			async: true,
			success: function(html) {
				if(title){
					//设定标题
					document.title = title;
				}
				
				//js(html);
				$(updateElem).html(html);
				if(Obj.done) {
					if(typeof html == 'object'){
						//如果是对象，返回code为2，把所有对象信息返回为json参数
						Obj.done({'code':2,'json':html})
					}else{
						Obj.done({'code':1,'html':html})
					}
					
				}
			},
			error: function(r) {
				if(Obj.done) {
					if(Obj.done({'code':0,'errmsg':r}) == false) {
						return;
					}
				}
				if(typeof(errorConfig)=="string"){
					//url执行...
					$.ajax({
						url:errorConfig,
						async:true,
						success:function(html){
							$(updateElem).html(html);
						}
					});
				}else if(typeof(errorConfig)=="object"){
					//obj执行...
					if(errorConfig.title){
						document.title = errorConfig.title;
					}
					$(updateElem).html(errorConfig.html);
				}else if(typeof(errorConfig)=="boolean"){
					//默认执行...
					if(errorConfig==true){
						errorConfig=config.errorConfig;
						if(errorConfig.title){
							document.title = errorConfig.title;
						}
						$(updateElem).html(errorConfig.html);
					}
				}
			}
		});
	}

	// 设置子页显示内容
	sapp.html = function(p1,p2) {
		if(p2){
			$(p1).html(p2);
		}else{
			$(config.elem).html(p1);
		}
	}
	
	// 局部加载
	sapp.partLoad = function(obj) {
		var elem = obj.elem || config.elem;
		var sign = obj.sign || false;
		if(sign==true){
			sign=config.sign;
		}
		sapp.render({
			subpageObj:{"url":obj.url}
			,sign:sign
			,done:function(resultObj){
				if(obj.done){
					obj.done(resultObj)
				}
			}
		})
	}
	
	//------------------------未验证
	/*
	 * 监听浏览器切换页面，如果跳转并加载页面，并定义现已加载的页面
	 * 
	 * 0.1.x
	 * 参数说明：bobj：更新浏览器历史记录，如果首次进入页面使用了cutSubpage方法，bobj必须为null，否则需要传入一个对象
	 * 				    对象存在参数，url 必选，title 可选(但是不会更改新标题内容)，其他自定义参数...
	 * 			jump：可以是一个function 或者是一个 bool ，如果jump返回为false，则不执行插件跳转，如果为true，执行插件跳转。
	 * 				    如果是function，在 function写每次跳转前执行的内容，需要return true;
	 * 				    不想使用插件跳转或自定义跳转，直接 return false;
	 * 			done：浏览器加载完成的回调。只有jump为true的时候才有回调。
	 * 0.2.x
	 * 参数说明：bobj：更新浏览器历史记录，如果首次进入页面使用了cutSubpage方法，bobj必须为null，否则需要传入一个对象
	 * 				    对象存在参数，url 必选，title 可选(但是不会更改新标题内容)，其他自定义参数...
	 * 			done：监听回调;
	 */
	sapp.browserListen = function(bObj,done) {
		
		if(bObj != null) {
			history.pushState(bObj, '', bObj.url);
		}
		
		window.addEventListener('popstate', function(obj) {
			if(done){
				done(obj);
			}
		})
		
		//旧------0.1.x版本
//		window.addEventListener('popstate', function(obj) {
//			var c =false ;
//			if(typeof jump == 'function'){
//				if(jump(obj)){
//					c=true
//				}
//			}else{
//				if(jump){
//					c=true
//				}
//			}
//			if(c==true){
//				var argObj = obj.state;
//				sapp.render({
//					subpageObj:argObj
//					,done:function(resultObj){
//						if(done){
//							done(robj);
//						}
//					}
//				})
//			}
//		})
	}
	
	

	/*
	 * 修改全局配置
	 */
	sapp.config = function(obj) {
		config.errorConfig = obj.errorConfig || config.errorConfig;
		config.elem = obj.elem || config.elem;
		config.sign = obj.sign || config.sign;
		
		return sapp;
	}

	/*
	 * 修改标题，立即生效
	 */
	sapp.title = function(c) {
		document.title = c;
		return sapp;
	}
	

	sapp.prototype.init.prototype = sapp.prototype;
	window.sapp = $app = sapp; //对外开放
})(window);

