<?php

/*
 *	执行.sql文件
 *  @param file 	sql文件名
 *  @param database 数据库名字（需要为空或不存在）
 *  @param prefix 	表前缀
 *  @param hostname ip地址
 *  @param username 用户名
 *  @param pwd 		密码
 */
function insert($file, $database, $prefix, $hostname, $username, $pwd) {

	//将表导入数据库
	header("Content-type: text/html; charset=utf-8");
	$_sql = file_get_contents($file);
	//写自己的.sql文件
	//替换前缀 {$ENIANADMIN_PREFIX}
	$_sql = str_replace('{$ENIANADMIN_PREFIX}', $prefix, $_sql);
	$_arr = explode(';', $_sql);
	$_mysqli = new mysqli($hostname, $username, $pwd);
	//第一个参数为域名，第二个为用户名，第三个为密码，第四个为数据库名字

	if (mysqli_connect_errno()) {
		jsonReturn(array('code' => 0, 'errmsg' => '数据库连接出错'));
	} else {
		//创建数据表
		$_mysqli -> query("CREATE DATABASE {$database}");
		//关闭连接
		$_mysqli -> close();
		$_mysqli = null;
		//重连数据库
		$_mysqli = new mysqli($hostname, $username, $pwd, $database);
		if (mysqli_connect_errno()) {
			jsonReturn(array('code' => 0, 'errmsg' => '数据库连接出错'));
		} else {
			//执行sql语句
			$_mysqli -> query('set names utf8;');
			//设置编码方式
			foreach ($_arr as $_value) {
				$_mysqli -> query($_value . ';');
				//var_dump($_value.'<br>');
			}
			jsonReturn(array('code' => 1));
		}

	}
	$_mysqli -> close();
	$_mysqli = null;
}

/*
 *	替换 database.php 中的连接信息，并生成新的文件
 * @param	rootPath	源文件目录
 * @param	buildPath	目标文件目录
 * @params	数据库，配置信息
 */
function buildDatabase($filePath, $buildPath, $hostname, $database, $username, $password, $hostport, $prefix) {
	//	file_exists('../../../application/database.php')

	//目标目录：../../../application/database.php
	$content = file_get_contents($filePath);
	//替换部分信息-
	$content = str_replace('{$ENIANADMIN_HOSTNAME}', $hostname, $content);
	$content = str_replace('{$ENIANADMIN_DATABASE}', $database, $content);
	$content = str_replace('{$ENIANADMIN_USERNAME}', $username, $content);
	$content = str_replace('{$ENIANADMIN_PASSWORD}', $password, $content);
	$content = str_replace('{$ENIANADMIN_HOSTPORT}', $hostport, $content);
	$content = str_replace('{$ENIANADMIN_PREFIX}', $prefix, $content);
	$content = str_replace('{$ENIANADMIN_DATABASE_BUILD_DATE}', date("Y-m-d H:i:s"), $content);
	//生成保存文件
	if (file_put_contents($buildPath, $content)) {
		jsonReturn(array('code' => 1));
	} else {
		jsonReturn(array('code' => 0));
	}

}

/*
 *	删除安装文件及文件夹
 *
 */
function delInstallFile($path) {
	//如果是目录则继续
	if (is_dir($path)) {
		//扫描一个文件夹内的所有文件夹和文件并返回数组
		$p = scandir($path);
		foreach ($p as $val) {
			//排除目录中的.和..
			if ($val != "." && $val != "..") {
				//如果是目录则递归子目录，继续操作
				if (is_dir($path . $val)) {
					//子目录中操作删除文件夹和文件
					delInstallFile($path . $val . '/');
					//目录清空后删除空文件夹
					@rmdir($path . $val . '/');
				} else {
					//如果是文件直接删除
					unlink($path . $val);
				}
			}
		}
	}
	//删除最后文件夹
	@rmdir($path.'/');
	return true;
}

/*
 * 返回json数据
 */
function jsonReturn($data) {
	header('Content-Type:application/json; charset=utf-8');
	echo json_encode($data);
}

/*
 * 入口函数
 */
function init() {
	//设置时区
	date_default_timezone_set('PRC');
	//$_GET['step']();
	switch ($_GET['step']) {
		case 1 :
			//执行sql文件
			insert("../core/enianAdmin.sql", $_POST['database'], $_POST['prefix'], $_POST['hostname'], $_POST['username'], $_POST['password']);
			break;
		case 2 :
			//创建数据库配置文件
			buildDatabase('../core/database.php', '../../../application/database.php', $_POST['hostname'], $_POST['database'], $_POST['username'], $_POST['password'], $_POST['hostport'], $_POST['prefix']);
			
			break;
		case 3 :
			//删除文件，如果不成功请手动删除下目录下所有文件。
			$path = '../../install/';
			if(delInstallFile($path)){
				if(!file_exists($path)){
					jsonReturn(array('code' => 1));
				}else{
					jsonReturn(array('code' => 0));
				}
			}
			break;
		default :
			break;
	}
}

//执行
@init();
