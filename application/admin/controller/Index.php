<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use app\admin\model\Getlognamedata;

class Index extends Common {

	public function test() {
		//可以在此处写测试代码
		$user = $this->getUserinfo();
		dump($user);
		// return false;
	}

	private function index() {
		// 因无内容index（后台首页）页面转向about（关于页面）
		
		return $this->about();
		// return view();//如果模板已更改可以解除本行注释，删除上行代码
	}

	public function login($type = 0) {
		
		switch ($type) {
			case 1 :
				$username = input('param.username');
				$password = input('param.password');
				$result = $this -> validate(array('username' => $username, 'password' => $password, ), array('username' => 'require|length:6,15', 'password' => 'require|length:6,15', ), array('username' => '账号必须大于6位小于15位', 'password' => '密码不能为空并且必须大于8位', ));
				if (true !== $result) {
					// 验证失败 输出错误信息
					return json(array('code' => 0, 'msg' => $result));
				}

				if (!captcha_check($_POST['verify'])) {
					return json(array('code'=>0,'msg'=>'验证码错误！'));
				}
				$where['username'] = $username;
				$where['password'] = md5($password);
				$where['status'] = 1;
				$admin_info = Db::name('admin') -> where($where) -> find();
				if ($admin_info) {
					//登录成功
					$cache_name = 'admin_key_aid_' . $admin_info['aid'];

					if (!$admin_info['loginkey']) {
						$loginkey = $this -> build_loginkey($admin_info['username'], $admin_info['password']);
					} else {
						$loginkey = $admin_info['loginkey'];
					}
					Db::name('admin') -> cache($cache_name, 0, 'admin_key') -> where('aid', $admin_info['aid']) -> update(array('loginkey' => $loginkey));
					$update['last_time'] = time();
					$update['last_ip'] = $_SERVER["REMOTE_ADDR"];
					Db::name('admin') -> where('aid', $admin_info['aid']) -> update($update);
					session('loginkey', $loginkey);
					session('userid', $admin_info['aid']);
					$this -> w_sys_log(1, array());
					return json(array('code' => 1));
				} else {
					session(NULL);
					$logContent['username'] = $username;
					$this -> w_sys_log(1, $logContent, 0);
					return json(array('code' => 0, 'msg' => '账号或密码错误！'));
				}

				break;

			default :
				return view();
				break;
		}

	}

	//生成验证码
	public function verify(){
		$config = config('captcha');
		$captcha = new \think\captcha\Captcha($config);
        return $captcha->entry();
	}

	//生成loginkey
	private function build_loginkey($username, $password) {
		return md5($username . $password . time());
	}

	private function update_admin_loginkey($aid) {
		$admin_info = Db::name('admin') -> where('aid', $aid) -> find();
		$update['loginkey'] = $this -> build_loginkey($admin_info['username'], $admin_info['password']);
		$cache_name = 'admin_key_aid_' . $admin_info['aid'];
		$result = Db::name('admin') -> cache($cache_name, 0, 'admin_key') -> where('aid', $admin_info['aid']) -> update($update);
		return $update['loginkey'];
	}

	//退出
	public function logout() {
		if (session('?loginkey')) {
			session('loginkey', NULL);
		}
		if (session('?userid')) {
			session('userid', NULL);
		}
		$this -> success('已成功退出登录，即将跳转登录页面', url('index/login'));
	}

	//修改密码
	private function updatePassword($t = 0) {
		switch ($t) {
			case 1 :
				//保存新密码
				$where['aid'] = session('userid');
				$where['password'] = md5($_POST['oldpwd']);
				$admin_info = Db::name('admin') -> where($where) -> find();
				if ($admin_info && $_POST['newpwd'] == $_POST['sureNewPwd']) {
					$newPassWord = md5($_POST['sureNewPwd']);
					$update['loginkey'] = $this -> build_loginkey($admin_info['username'], $newPassWord);
					$update['password'] = $newPassWord;
					$cache_name = 'admin_key_aid_' . $admin_info['aid'];
					$result = Db::name('admin') -> cache($cache_name, 0, 'admin_key') -> where('aid', $where['aid']) -> update($update);
					if ($result) {
						$msg = '密码更改成功，当前登录即将退出登录';

					} else {
						$msg = '密码没有改动';
					}
					return json(array('code' => $result, 'msg' => $msg));
				} else {
					return json(array('code' => 0, 'msg' => '旧密码不正确,或者两次新密码输入不一致'));
				}
				break;

			default :
				return view('updatePassword');
				break;
		}
		$this -> logout();
		$this -> success('已成功退出登录，即将跳转登录页面', url('index/login'));
	}

	//左侧菜单开发
	private function o_setMenuLeft($t = 0, $menuType = 0) {
		$aid = session('userid');
		//dump($menuType);
		$t = input('param.type');
		$menuType = input('param.menuType');
		switch ($t) {

			case 1 :
				//新增主菜单
				$data['img'] = input('param.img') ? input('param.img') : '';
				$data['title'] = input('param.title');
				$data['id'] = input('param.m_id');
				//上级id
				$data['note'] = 0;
				$data['status'] = 1;
				//$data['aid'] = $aid;
				$data['type'] = 1;

				$r = Db::name('menu') -> insert($data);

				return json(array('code' => $r));
				break;
			case 2 :
				//新增子菜单
				$data['img'] = input('param.img') ? input('param.img') : '';
				$data['title'] = input('param.title');
				$data['id'] = input('param.m_id');
				//上级id
				$data['note'] = input('param.note');
				$data['status'] = 1;
				$data['url'] = input('param.url');
				//$data['aid'] = $aid;
				$data['type'] = 1;
				//打开类型储存在note2中，该数据为json字符串
				$data['note2'] = json_encode(array('type' => input('param.opentype')));

				$r = Db::name('menu') -> insert($data);
				return json(array('code' => $r));
				break;

			case 3 :
				//修改主
				$update['img'] = input('param.img') ? input('param.img') : '';
				$update['title'] = input('param.title');
				$update['id'] = input('param.m_id');
				$r = Db::name('menu') -> where('id', input('param.id')) -> update($update);
				return json(array('code' => $r));
				break;
			case 4 :
				$update['img'] = input('param.img') ? input('param.img') : '';
				$update['title'] = input('param.title');
				$update['url'] = input('param.url');
				$update['id'] = input('param.m_id');
				$update['note2'] = json_encode(array('type' => input('param.opentype')));
				$r = Db::name('menu') -> where('id', input('param.id')) -> update($update);
				return json(array('code' => $r));
				break;
			case 5 :
				//删除菜单
				$getId = input('param.ids/a');
				$r = Db::name('menu') -> delete($getId);
				return json(array('code' => 1, 'count' => $r));
				break;
			case 6 :
				//				//加载菜单数据
				//				//$where['aid'] = $aid;
				//				$where['type'] = 0;
				//				//$where['type'] = 1;
				//
				//				$data = Db::name('menu') -> where($where) -> select();
				//				foreach ($data as $k => $v) {
				//					$data[$k]['pid'] = $v['note'];
				//					$json = json_decode($v['note2'], true);
				//					$data[$k]['type'] = $json['type'];
				//					unset($data[$k]['note'], $data[$k]['note2']);
				//				}
				//				//$data=json_encode($data);
				//				return json(array('code' => 1, 'data' => $data));
				break;
			case 7 :
				//修改排序
				$where['id'] = input('param.id');
				$update[input('param.field')] = input('param.value');
				$r = Db::name('menu') -> where($where) -> update($update);

				return json(array('code' => $r));
				break;
			default :
				//$where['aid'] = $aid;
				if ($menuType == 1) {
					$where['type'] = 1;
				} else {
					$where = "";
				}
				$data = Db::name('menu') -> where($where) -> order('order asc') -> select();
				foreach ($data as $k => $v) {
					$data[$k]['pid'] = $v['note'];
					$json = json_decode($v['note2'], true);
					$data[$k]['type'] = $json['type'];
					unset($data[$k]['note'], $data[$k]['note2']);
				}
				$data = json_encode($data);
				return view("o_setMenuLeft", array('data' => $data));

				break;
		}

	}

	//左侧菜单设置
	private function setMenuLeft($t = 0) {
		$aid = session('userid');
		$t = input('param.type');
		switch ($t) {

			case 1 :
				//新增主菜单
				$data['img'] = input('param.img') ? input('param.img') : '';
				$data['title'] = input('param.title');
				//上级id
				$data['note'] = 0;
				$data['status'] = 1;
				$data['aid'] = $aid;
				$data['type'] = 0;

				$r = Db::name('menu') -> insert($data);

				return json(array('code' => $r));
				break;
			case 2 :
				//新增子菜单
				$data['img'] = input('param.img') ? input('param.img') : '';
				$data['title'] = input('param.title');
				//上级id
				$data['note'] = input('param.note');
				$data['status'] = 1;
				$data['url'] = input('param.url');
				$data['aid'] = $aid;
				$data['type'] = 0;
				//打开类型储存在note2中，该数据为json字符串
				$data['note2'] = json_encode(array('type' => input('param.opentype')));

				$r = Db::name('menu') -> insert($data);
				return json(array('code' => $r));
				break;

			case 3 :
				//修改主
				$update['img'] = input('param.img') ? input('param.img') : '';
				$update['title'] = input('param.title');
				$r = Db::name('menu') -> where('id', input('param.id')) -> update($update);
				return json(array('code' => $r));
				break;
			case 4 :
				//修改子
				$update['img'] = input('param.img') ? input('param.img') : '';
				$update['title'] = input('param.title');
				$update['url'] = input('param.url');
				$update['note2'] = json_encode(array('type' => input('param.opentype')));
				$r = Db::name('menu') -> where('id', input('param.id')) -> update($update);
				return json(array('code' => $r));
				break;
			case 5 :
				//删除菜单
				$getId = input('param.ids/a');
				$r = Db::name('menu') -> delete($getId);
				return json(array('code' => 1, 'count' => $r));
				break;
			case 6 :
				//加载菜单数据
				$where['aid'] = $aid;
				$where['type'] = 0;

				$data = Db::name('menu') -> where($where) -> select();
				foreach ($data as $k => $v) {
					$data[$k]['pid'] = $v['note'];
					$json = json_decode($v['note2'], true);
					$data[$k]['type'] = $json['type'];
					unset($data[$k]['note'], $data[$k]['note2']);
				}
				return json(array('code' => 1, 'data' => $data));
				break;
			case 7 :
				//加载菜单数据-暂未使用
				$where['id'] = input('param.id');
				$update[input('param.field')] = input('param.value');
				$r = Db::name('menu') -> where($where) -> update($update);

				return json(array('code' => $r));
				break;
				
			case 8 :
				//拷贝默认菜单到该用户，前台：添加默认
				
				$where['type']=1;
				$where['note']=0;
				$menuData = Db::name('menu')->where($where)->select();
				foreach ($menuData as $k => $v) {
					unset($menuData[$k]['id']);
					$menuData[$k]['aid']=$aid;
					$menuData[$k]['type']=0;
					$getId = Db::name('menu')->insertGetId($menuData[$k]);
					$whereSon['note']=$v['id'];
					
					$getMenuSonData = Db::name('menu')->where($whereSon)->select();
					foreach ($getMenuSonData as $key => $value) {
						unset($getMenuSonData[$key]['id']);
						$getMenuSonData[$key]['note']=$getId;
						$getMenuSonData[$key]['aid']=$aid;
						$getMenuSonData[$key]['type']=0;
						Db::name('menu') -> insert($getMenuSonData[$key]);
					}
				}
				

				return json(array('code' => 1,'data'=>''));
				break;
			default :
				$where['aid'] = $aid;
				$where['type'] = 0;

				$data = Db::name('menu') -> where($where) -> order('order asc') -> select();
				foreach ($data as $k => $v) {
					$data[$k]['pid'] = $v['note'];
					$json = json_decode($v['note2'], true);
					$data[$k]['type'] = $json['type'];
					unset($data[$k]['note'], $data[$k]['note2']);
				}
				$data = json_encode($data);
				//$this -> view -> engine -> layout('layout/page');
				return view("setMenuLeft", array('data' => $data));

				break;
		}

	}

	//管理员权限设置
	private function setAdminAuth($t = 0) {
		$aid = input('param.aid');
		$t = input('param.type');
		switch ($t) {

			case 1 :
				//添加管理员权限
				$data = input('param.data/a');
				$authStr = Db::name('admin') -> where('aid', input('param.aid')) -> value('auth');
				$authArr = json_decode($authStr, true);
				$authArr = is_array($authArr) ? $authArr : array();
				$newAuthArr = array_merge(is_array($authArr) ? $authArr : array(), $data);
				$update['auth'] = json_encode($newAuthArr);
				$r = Db::name('admin') -> where('aid', input('param.aid')) -> update($update);
				return json(array('code' => 1, 'msg' => $update['auth']));
				break;
			case 2 :
				//删除管理员权限
				$data = input('param.data/a');
				$authStr = Db::name('admin') -> where('aid', input('param.aid')) -> value('auth');
				$authArr = json_decode($authStr, true);
				$authArr = is_array($authArr) ? $authArr : array();
				$newAuthArr = array_diff($authArr, $data);
				$update['auth'] = json_encode($newAuthArr);
				$r = Db::name('admin') -> where('aid', input('param.aid')) -> update($update);

				return json(array('code' => 1, 'msg' => $update['auth']));
				break;

			default :
				//先取出主应用数据
				//更改主应用数据 appid -》id   pid=0
				//取出子应用数据
				//更改主应用数据appid -》pid  sid = id
				//取出被设置管理员的权限id表，并查出是否有权限并增加字段pass 为1 或 0
				//合并主应用和子应用数据
				$where['aid'] = $aid;
				//$where['type'] = 0;
				//$where['type'] = 1;

				$appsData = Db::name('apps') -> order('order asc') -> select();

				foreach ($appsData as $k => $v) {
					$appsData[$k]['pid'] = 0;
					$appsData[$k]['id'] = 'g_' . $v['appid'];
					$appsData[$k]['url'] = '';
					//$appsData[$k]['url'] ='';
					$appsData[$k]['pass'] = '';
					//$appsData[$k]['url'] ='';
					unset($appsData[$k]['appid']);
				}
				$appsData[] = array('id' => 'g_0', 'pid' => 0, 'name' => '系统核心');
				$son_appsData = Db::name('son_apps') -> where('is_auth', 1) -> order('order asc') -> select();

				foreach ($son_appsData as $k => $v) {
					$auths = Db::name('admin') -> where($where) -> value('auth');
					$auths = json_decode($auths, true);
					if (is_array($auths) && in_array($v['sid'], $auths)) {
						$son_appsData[$k]['pass'] = 1;
					} else {
						$son_appsData[$k]['pass'] = 0;
					}
					$son_appsData[$k]['pid'] = 'g_' . $v['appid'];
					$son_appsData[$k]['id'] = $v['sid'];
					unset($son_appsData[$k]['appid'], $son_appsData[$k]['is_display'], $son_appsData[$k]['field'], $son_appsData[$k]['is_auth'], $son_appsData[$k]['sid']);
				}
				$data = json_encode(array_merge($appsData, $son_appsData));
				return view("setAdminAuth", array('data' => $data));

				break;
		}

	}
	
	//设置用户的用户组
	private function setAdminAuthGroup($t = 0){
		$aid = input('param.aid');
		$t = input('param.type');
		$table = 'user_group';
		switch ($t) {

			case 1 :
				//更新
				$data = input('param.data/a');
				if($data){
					$update['group'] = json_encode($data);
				}else{
					$update['group'] = '';
				}
				
				$r = Db::name('admin') -> where('aid', $aid) -> update($update);
				return json(array('code' => 1, 'msg' => $update['group']));
				break;
			
			default :
				$getData = Db::name($table)->select();
				$groupStr = Db::name('admin') -> where('aid', input('param.aid')) -> value('group');
				$groupArr = json_decode($groupStr, true);
				$groupArr = is_array($groupArr) ? $groupArr : array();
				$groupArr=json_encode($groupArr);
				$data = json_encode($getData);
				//dump($data);
				return view("setAdminAuthGroup", array('data' => $data,'checked'=>$groupArr));

				break;
		}
	}
	
	//用户组列表
	private function userGroupList($t = 0) {
		$table = 'user_group';

		switch ($t) {
			case 1 :
				//获得数据
				$data = select_table_data($table);
				$getData = $data['data'];
				$getDataCount = $data['count'];
				return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
				break;
			case 2 :
				//修改
				$where['id'] = input('param.id');
				$update[input('param.field')] = input('param.value');
				
				$r = Db::name($table) -> where($where) -> update($update);
				
				return json(array('code' => $r));
				break;
			case 3 :
				//删除
				$where['id'] = input('param.id');
				$r = Db::name($table) -> where($where) -> delete();
				return json(array('code' => $r));
				break;
			case 4 :
				//添加用户组
				$data['name']=input('param.name');
				$data['note']=input('param.note');
				$data['status']=input('param.status');
				$r = Db::name($table)->insert($data);
				
				return json(array('code' => $r));
				break;
			default :
				return view("userGroupList");
				break;
		}
		
		


	}

	private function setUserGroup($t = 0) {
				
		$id = input('param.id');
		$t = input('param.type');
		$table = 'user_group';
		switch ($t) {

			case 1 :
				//添加权限
				$data = input('param.data/a');
				$authStr = Db::name($table) -> where('id', $id) -> value('auth');
				$authArr = json_decode($authStr, true);
				$authArr = is_array($authArr) ? $authArr : array();
				$newAuthArr = array_merge(is_array($authArr) ? $authArr : array(), $data);
				$update['auth'] = json_encode($newAuthArr);
				$r = Db::name($table) -> where('id', $id) -> update($update);
				return json(array('code' => $r, 'msg' => $update['auth']));
				break;
			case 2 :
				//删除权限
				$data = input('param.data/a');
				$authStr = Db::name($table) -> where('id', $id) -> value('auth');
				$authArr = json_decode($authStr, true);
				$authArr = is_array($authArr) ? $authArr : array();
				$newAuthArr = array_diff($authArr, $data);
				$update['auth'] = json_encode($newAuthArr);
				$r = Db::name($table) -> where('id', $id) -> update($update);

				return json(array('code' => $r, 'msg' => $update['auth']));
				break;

			default :
				//先取出主应用数据
				//更改主应用数据 appid -》id   pid=0
				//取出子应用数据
				//更改主应用数据appid -》pid  sid = id
				//取出被设置管理员的权限id表，并查出是否有权限并增加字段pass 为1 或 0
				//合并主应用和子应用数据
				$where['id'] = $id;
				//$where['type'] = 0;
				//$where['type'] = 1;

				$appsData = Db::name('apps') -> order('order asc') -> select();

				foreach ($appsData as $k => $v) {
					$appsData[$k]['pid'] = 0;
					$appsData[$k]['id'] = 'g_' . $v['appid'];
					$appsData[$k]['url'] = '';
					//$appsData[$k]['url'] ='';
					$appsData[$k]['pass'] = '';
					//$appsData[$k]['url'] ='';
					unset($appsData[$k]['appid']);
				}
				// $appsData[] = array('id' => 'g_0', 'pid' => 0, 'name' => '系统核心');
				$son_appsData = Db::name('son_apps') -> where('is_auth', 1) -> order('order asc') -> select();

				foreach ($son_appsData as $k => $v) {
					$auths = Db::name($table) -> where($where) -> value('auth');
					$auths = json_decode($auths, true);
					if (is_array($auths) && in_array($v['sid'], $auths)) {
						$son_appsData[$k]['pass'] = 1;
					} else {
						$son_appsData[$k]['pass'] = 0;
					}
					$son_appsData[$k]['pid'] = 'g_' . $v['appid'];
					$son_appsData[$k]['id'] = $v['sid'];
					unset($son_appsData[$k]['appid'], $son_appsData[$k]['is_display'], $son_appsData[$k]['field'], $son_appsData[$k]['is_auth'], $son_appsData[$k]['sid']);
				}
				$data = json_encode(array_merge($appsData, $son_appsData));
				return view("setUserGroup", array('data' => $data));

				break;
		}
	}
	//应用列表
	private function applist($t = 0) {
		switch ($t) {
			case 1 :
				//获得数据
				$data = select_table_data('apps');
				$getData = $data['data'];
				$getDataCount = $data['count'];
				return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
				break;
			case 2 :
				//修改
				$where['appid'] = input('param.appid');
				$update[input('param.field')] = input('param.value');

				$r = Db::name('apps') -> where($where) -> update($update);
				//写日志
				$logContent['log_content_type'] = 3;
				$logContent['appid'] = input('param.appid');
				$logContent[input('param.field')] = input('param.value');
				$this -> w_sys_log(4, $logContent);
				return json(array('code' => $r));
				break;
			case 3 :
				//删除

				$where['appid'] = input('param.appid');
				$appInfo = Db::name('apps') -> where($where) -> find();
				$r = Db::name('apps') -> where($where) -> delete();
				//删除所有子应用-18/12/15
				Db::name('son_apps') -> where($where) -> delete();
				//写日志
				$logContent['log_content_type'] = 2;
				$logContent['appid'] = $appInfo['appid'];
				$logContent['name'] = $appInfo['name'];
				$logContent['note'] = $appInfo['note'];
				$this -> w_sys_log(4, $logContent);
				return json(array('code' => $r));
				break;

			default :
				return view("");
				break;
		}

	}

	//应用列表
	private function myAppList($t = 0) {
		switch ($t) {
			case 1 :
				//加载子应用
				$where['is_display'] = 1;
				$where['type'] = array('!=',1);
				$where['appid'] = input('param.appid');
				$sonAppData = Db::name('son_apps') -> where($where) -> select();
				$getDataCount = 0;
				foreach ($sonAppData as $k => $v) {
					$sonAppData[$k]['url'] = '/' . $v["url"] . '.html';
				}
				return json(array('code' => 0, 'data' => $sonAppData, 'count' => $getDataCount));
				break;
			case 2 :
				//批量加载子应用
				$appids = input('param.appids/a');
				$data = array();
				$where['is_display'] = 1;
				for ($i = 0; $i < count($appids); $i++) {
					$where['appid'] = $appids[$i];
					$data[$appids[$i]] = Db::name('son_apps') -> where($where) -> select();
					foreach ($data[$appids[$i]] as $k => $v) {
						$data[$appids[$i]][$k]['url'] = '/' . $v["url"] . '.html';
					}

				}
				return json(array('code' => 0, 'data' => $data));
				break;
			default :
				$appData = Db::name('apps') -> select();
				return view("myAppList", array('data' => json_encode($appData)));
				break;
		}

	}

	//应用列表
	private function setting($t = 0) {
		switch ($t) {
			case 1 :
				//加载子应用
				$sonAppData = Db::name('son_apps') -> where('appid', input('param.appid')) -> select();
				$getDataCount = 0;
				//dump($sonAppData);
				return json(array('code' => 0, 'data' => $sonAppData, 'count' => $getDataCount));
				break;

			default :
				$appData = Db::name('apps') -> select();
				//dump($appData);
				return view("setting", array('data' => json_encode($appData)));
				break;
		}

	}

	//管理员列表
	private function adminList($t = 0) {
		$table = 'admin';

		switch ($t) {
			case 1 :
				//获得数据
				$data = select_table_data($table);
				$getData = $data['data'];
				$getDataCount = $data['count'];
				foreach ($getData as $k => $v) {
					$getData[$k]['password'] = '******';
					$getData[$k]['start_time'] = date("Y-m-d H:i:s", $v['start_time']);
					$getData[$k]['last_time'] = !$v['last_time'] ? '未登录过' : date("Y-m-d H:i:s", $v['last_time']);
				}
				return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
				break;
			case 2 :
				//修改
				$where['aid'] = input('param.aid');
				$update[input('param.field')] = input('param.value');
				//定义不能更新的字段
				$unUpdate = array('start_time', 'end_time', 'last_time', 'last_ip');
				if (in_array(input('param.field'), $unUpdate)) {
					return json(array('code' => 0, 'msg' => '抱歉，不能修改此字段中的内容！！！'));
				}

				if (input('param.field') == 'password') {
					$password = input('param.value');
					//验证，只有密码
					$result = $this -> validate(array('password' => $password), array('password' => 'require|length:6,15'), array('password' => '密码不能为空并且必须大于8位'));
					if (true !== $result) {
						// 验证失败 输出错误信息
						return json(array('code' => 0, 'msg' => $result));
					}
					$update['password'] = md5($password);
				}
				$r = Db::name($table) -> where($where) -> update($update);
				if (input('param.field') == 'password') {
					$this -> update_admin_loginkey($where['aid']);
				}
				if (input('param.field') == 'status') {
					$this -> update_admin_loginkey($where['aid']);
				}
				//写日志
				$logContent['log_content_type'] = 3;
				$logContent['aid'] = input('param.aid');
				if (input('param.field') == 'password') {
					//由于密码为md5加密所以此处伪装星号，md5加密值不会暴露
					$logContent['password'] = '******';
				} else {
					$logContent[input('param.field')] = input('param.value');
				}

				$this -> w_sys_log(2, $logContent);
				return json(array('code' => $r));
				break;
			case 3 :
				//删除

				$where['aid'] = input('param.aid');
				$adminInfo = Db::name($table) -> where($where) -> find();
				$r = Db::name($table) -> where($where) -> delete();
				//同时删除菜单内容
				Db::name('menu') -> where($where) -> delete();
				//写日志		[aid,username,name,note]
				$logContent['log_content_type'] = 2;
				$logContent['aid'] = $adminInfo['aid'];
				$logContent['username'] = $adminInfo['username'];
				$logContent['name'] = $adminInfo['name'];
				$logContent['note'] = $adminInfo['note'];
				$this -> w_sys_log(2, $logContent);
				return json(array('code' => $r));
				break;

			default :
				return view("adminList");
				break;
		}

	}

	//添加管理员
	private function addAdmin($t = 0) {
		switch ($t) {
			case 1 :
				//提交
				$data['name'] = input('param.name') ? input('param.name') : '未设定名字';
				$data['username'] = input('param.username');
				$password = input('param.password');
				//验证
				$result = $this -> validate(array('username' => input('param.username'), 'password' => $password, ), array('username' => 'require|length:6,15', 'password' => 'require|length:6,15', ), array('username' => '账号必须大于6位小于15位', 'password' => '密码不能为空并且必须大于8位', ));
				if (true !== $result) {
					// 验证失败 输出错误信息
					return json(array('code' => 0, 'msg' => $result));
				}

				$data['password'] = md5($password);
				$data['note'] = input('param.note');
				$data['status'] = input('param.status');
				$data['start_time'] = time();

				$r = Db::name('admin') -> insert($data);

				//写日志 //[aid,username,name,note,status]
				$logContent['log_content_type'] = 1;
				$logContent['aid'] = Db::name('admin') -> getLastInsID();
				$logContent['status'] = input('param.status');
				$logContent['note'] = input('param.note');
				$logContent['username'] = input('param.username');
				$logContent['name'] = input('param.name') ? input('param.name') : '未设定名字';
				$this -> w_sys_log(2, $logContent);
				return json(array('code' => $r));
				break;

			default :
				return view("addAdmin");
				break;
		}
	}

	//子应用信息编辑
	private function sonAppInfoEdit($t = 0) {
		switch ($t) {
			case 1 :
				//获得数据
				$getData = Db::name('apps') -> select();
				$getDataCount = Db::name('apps') -> count();
				return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
				break;
			case 2 :
				//修改
				$where['appid'] = input('param.appid');
				$update[input('param.field')] = input('param.value');

				$r = Db::name('apps') -> where($where) -> update($update);

				return json(array('code' => $r));
				break;
			case 3 :
				//删除

				$where['appid'] = input('param.appid');
				$r = Db::name('apps') -> where($where) -> delete();
				return json(array('code' => $r));
				break;

			default :
				return view("sonAppInfoEdit");
				break;
		}

	}

	//子应用列表
	private function sonApplist($t = 0) {
		switch ($t) {
			case 1 :
				//获得数据
				$where['appid'] = input('param.appid');
				$data = select_table_data('son_apps',$where);
				$getData = $data['data'];
				$getDataCount = $data['count'];
				return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
				break;
			case 2 :
				//修改
				$where['sid'] = input('param.sid');
				$update[input('param.field')] = input('param.value');

				$r = Db::name('son_apps') -> where($where) -> update($update);
				//写日志
				$logContent['log_content_type'] = 6;
				$logContent['sid'] = input('param.sid');
				$logContent[input('param.field')] = input('param.value');
				$this -> w_sys_log(4, $logContent);
				return json(array('code' => $r));
				break;
			case 3 :
				//删除

				$where['sid'] = input('param.sid');
				$son_appInfo = Db::name('son_apps') -> where($where) -> find();
				$r = Db::name('son_apps') -> where($where) -> delete();

				//写日志 18/12/16
				$logContent['log_content_type'] = 5;
				$logContent['appid'] = $son_appInfo['appid'];
				$logContent['sid'] = $son_appInfo['sid'];
				$logContent['name'] = $son_appInfo['name'];
				$logContent['note'] = $son_appInfo['note'];
				$this -> w_sys_log(4, $logContent);
				return json(array('code' => $r));
				break;

			default :
				return view("sonApplist");
				break;
		}

	}

	//添加应用
	private function addApp($t = 0) {
		switch ($t) {
			case 1 :
				//提交
				$r = Db::name('apps') -> insert($_POST);
				//写日志
				$logContent['log_content_type'] = 1;
				$logContent['appid'] = Db::name('apps') -> getLastInsID();
				$logContent['name'] = input('param.name') ? input('param.name') : '未设定名字';
				$this -> w_sys_log(4, $logContent);
				return json(array('code' => $r));
				break;

			default :
				return view("addApp");
				break;
		}
	}

	//添加应用
	private function addSonApp($t = 0) {
		switch ($t) {
			case 1 :
				//提交
				$_POST['appid'] = input('param.appid');
				$_POST['type'] = input('param.type');
				$_POST['url'] = input('param.url');
				$r = Db::name('son_apps') -> insert($_POST);
				//写日志 18/12/16
				$logContent['log_content_type'] = 4;
				$logContent['sid'] = Db::name('son_apps') -> getLastInsID();
				$logContent['appid'] = input('param.appid');
				$logContent['name'] = input('param.name') ? input('param.name') : '未设定名字';
				$this -> w_sys_log(4, $logContent);
				return json(array('code' => $r));
				break;
			default :
				return view("addSonApp");
				break;
		}

	}

	//写系统日志
	/*
	 * 1.用户登录
	 * 2.用户管理
	 * 		1.用户增加。[aid,username,name,note]
	 * 		2.用户删除。[aid,username,name,note]
	 * 		3.用户修改。[aid,...]
	 * 3.权限操作
	 *
	 * 4.应用操作
	 * 		1.应用增加{appid,name,note}
	 * 		2.应用删除{appid,name,note}
	 * 		3.应用修改{appid,name,...}
	 * 		4.子应用增加{appid,sid,name,note,url,type}
	 * 		5.子应用删除{appid,sid,name,note,url,type}
	 * 		6.子应用修改{appid,sid,...}
	 */
	private function w_sys_log($type, $content, $status = 1) {
		$data['aid'] = session('userid');
		$data['type'] = $type;
		$data['content'] = json_encode($content);
		$data['status'] = $status;
		$data['time'] = time();
		$data['ip'] = $_SERVER["REMOTE_ADDR"];
		return Db::name('sys_log') -> insert($data);
	}

	//日志页
	private function sysLogList($t = 0) {
		switch ($t) {
			case 1 :
				//获得数据
				$Getlognamedata = new Getlognamedata();
				$data = select_table_data('sys_log',null,'time desc');
				$getData = $data['data'];
				$getDataCount = $data['count'];
				foreach ($getData as $k => $v) {
					$getData[$k]['time'] = date("Y-m-d H:i:s", $v['time']);
					//$getData[$k]['adminname'] = Db::name('admin')->where('aid',$v['aid']);
					$getData[$k]['aid'] = "" . $v['aid'] . " - " . Db::name('admin') -> where('aid', $v['aid']) -> value('name');
					//dump($v['type']);
					$getData[$k]['content'] = $Getlognamedata -> getContent($v['type'], $v['content']);
				}
				return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
				break;
			
			case 2:
				$time = input('param.time/a');
				$start = strtotime($time['start']);
				$end = strtotime($time['end'].' +1 month -1 second');
				$where['time']=array('between',array($start,$end));
				$r = Db::name('sys_log')->where($where)->delete();
				return json(array('code' => 1, 'count' => $r));
				break;
				
			default :
				return view("sysLogList");
				break;
		}
	}


	private function about() {
		@$content = file_get_contents('http://enianteam.com/enian_admin/index/get?v='.config('enianAdmin.v'));
		$data['enian_admin_version'] = config('enianAdmin.v');
		$data['system_version'] = php_uname('s');
		$data['thinkphp_version'] = THINK_VERSION;
		$data['php_version'] = PHP_VERSION;
		$mysqlInfo = Db::query("select VERSION() as ver");
		$data['mysql_version'] = $mysqlInfo[0]['ver'];
		$data['ifarme_src'] = ($_SERVER["SERVER_PORT"]==80?"http":"https").'://enianteam.com/enian_admin/index/iframe.html';
		$data['update_log'] = 'http://enianteam.com/enian_admin/index/update_log.html';
		$url['enianAdmin']="http://enianteam.com/enian_admin.html";
		$url['doc']="http://doc.enianteam.com/enian_admin.html";
		$url['mayun']="https://gitee.com/hslr/enian_admin";

		$data['url']=$url; 
		return view('about',$data);
	}

	//将所有页面指向此操作，
	public function _empty($name) {
		if(input('param.t')){
			return $this ->$name(input('param.t'));
		}
		
		//以下代码必须建在每个控制器的_empty的操作中
		$func = $this->loadHomeTpl($name);
		if(is_object($func)){
			return $func;
		}else{
			return $this->$func();
		}
		
		
	}

}
