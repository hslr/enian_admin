<?php
namespace app\admin\controller;


use think\Controller;
use think\Db;

// 这是 示例应用 的后台控制器


// 新建控制器需要继承Common才能实现登录和权限功能
class Example extends Common {
	
	public $table_cont = "app_example_content";


	// [页面] 网站配置页面 http://您后台域名/example/config.html
	// (需要将页面类型方法设定为私有(private)转到 _empty 方法中，_empty组合页面布局代码)
	private function index(){
		$content = '<h2>enianAdmin 使用手册【必读很重要】</h2><h3><a href="http://doc.enianteam.com/enian_admin/66.html" target="_blank" class="card-block-text" >http://doc.enianteam.com/enian_admin/66.html</a></h3>';
		return $content;
	}

	// [页面] 网站配置页面 http://您后台域名/example/config.html
	// (需要将页面类型方法设定为私有(private)转到 _empty 方法中，_empty组合页面布局代码)
	private function config(){
		$return_data = Db::name('app_example_config')->column('value','name');
		return view('',$return_data);
	}

	// [接口] 保存网站配置
	// 接口不需要设定为私有，一般会返回json数据。当然也可以给接口设定个特定参数，在_empty方法最前边写规则
	public function saveConfig(){
		$welcome_text = input("post.welcome_text");
		$title = input("post.title");
		Db::name('app_example_config')->where('name','title')->update(['value'=>$title]);
		Db::name('app_example_config')->where('name','welcome_text')->update(['value'=>$welcome_text]);
		return json(['code'=>1]);
	}

	// 快捷入口页面相关操作

	// [页面] 快捷入口设置页面
	private function ent(){
		return view('');
	}

	// [接口] 获得快捷入口表格数据（支持页码查询等）
	public function getEntData(){
		// select_table_data是配合前端封装的查询函数，支持页码，排序
		// 请参考：http://doc.enianteam.com/enian_admin/73.html
		// 查出type=0的数据（快捷入口数据）
		$data = select_table_data($this->table_cont,['type'=>0]);
		$getData = $data['data'];
		$getDataCount = $data['count'];
		// 因为模板需传code参数并且为0才识别为正确，此处传0
		return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
	}

	// [接口] 添加
	public function addEntData(){
		$data['title'] = input('param.title');
		$data['url'] = input('param.url');
		$data['type'] = 0;
		$r = Db::name($this->table_cont)->insert($data);
		return json(array('code' => $r));
	}

	// [接口] 修改数据
	public function updateEntData(){
		$where['id'] = input('param.id');
		$update[input('param.field')] = input('param.value');
		$r = Db::name($this->table_cont) -> where($where) -> update($update);
		return json(array('code' => $r));
	}

	// [接口] 删除
	public function delEntData(){
		//删除
		$where['id'] = input('param.id');
		$r = Db::name($this->table_cont) -> where($where) -> delete();
		return json(array('code' => $r));
	}

	// 内容页面相关操作

	// [页面] 快捷入口设置页面
	private function content(){
		return view('');
	}

	// [接口] 获得快捷入口表格数据（支持页码查询等）
	public function getContentData(){
		// select_table_data是配合前端封装的查询函数，支持页码，排序
		// 请参考：http://doc.enianteam.com/enian_admin/73.html
		// 查出type=1的数据（内容数据）
		$data = select_table_data($this->table_cont,['type'=>1]);
		$getData = $data['data'];
		$getDataCount = $data['count'];
		// 因为模板需传code参数并且为0才识别为正确，此处传0
		return json(array('code' => 0, 'count' => $getDataCount, 'data' => $getData));
	}

	// [接口] 添加
	public function addContentData(){
		$data['title'] = input('param.title');
		$data['url'] = input('param.url');
		$data['type'] = 1;
		$r = Db::name($this->table_cont)->insert($data);
		return json(array('code' => $r));
	}

	// [接口] 修改数据
	public function updateContentData(){
		$where['id'] = input('param.id');
		$update[input('param.field')] = input('param.value');
		$r = Db::name($this->table_cont) -> where($where) -> update($update);
		return json(array('code' => $r));
	}

	// [接口] 删除
	public function delContentData(){
		//删除
		$where['id'] = input('param.id');
		$r = Db::name($this->table_cont) -> where($where) -> delete();
		return json(array('code' => $r));
	}

	// 必须保留的方法操作
	
	// 将所有(private)私有页面指向此操作
	public function _empty($name) {
		// 可以在此处写您的个人规则
		// 下面规则是当[get|post|地址参数]参数中包含't'值，然后将值传入到指定操作方法，更多写法自己开发
		// 下面这种写法应用到了index控制器（系统应用中）example未应用到所以注释掉
		// if(input('param.t')){
		// 	return $this ->$name(input('param.t'));
		// }
		
		//以下代码必须建在每个控制器的_empty的操作中，(如果按框架写法)且不能改动
		$func = $this->loadHomeTpl($name);
		if(is_object($func)){
			return $func;
		}else{
			return $this->$func();
		}
		
		
	}
}
?>