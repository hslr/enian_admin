<?php
namespace app\admin\controller;


use think\Controller;
use think\Db;


class File extends Common {
	
	private function uploadFileManager(){
		return view('uploadFileManager');
	} 
	//获取目录内容
	public function getPathData(){
		if(input('param.dirId')){
			if(input('param.dirId')=='ROOT' || input('param.dirId')==0){
				$dirId['dirId']=0;
			}else{
				$dirId['dirId']=input('param.dirId');
			}
		}else{
			$dirId['dirId']=0;
		}
		$page = input('param.page');
		$limit = input('param.limit')?input('param.limit'):50;
		$start = input('param.page')==1?0:input('param.page')*$limit-$limit;
		$where['pid']=$dirId['dirId'];
		$where['type']='/DIR';
		$getDirData = Db::name('pic_lib')->where($where)->order('name asc')->select();
		$where['type']=array('<>','/DIR');
		$getFileData = Db::name('pic_lib')->where($where)->order('name asc')->select();
		$getAllData = array_merge($getDirData,$getFileData);
		$getNeedData = array_slice($getAllData,$start,$limit);
		return json(array('code'=>1,'fileList'=>$getNeedData,'count'=>count($getAllData)));
	}
	
	public function upload(){
		$returnArr=array();
		$uploadPath=ROOT_PATH . 'public' . DS . 'uploads';
		$getPid['dirId'] = input('param.id');
		if($getPid){
			 // 获取表单上传文件 例如上传了001.jpg
		    $file = request()->file('file');
		    // 移动到框架应用根目录/public/uploads/ 目录下
		    // 后缀限制绝对不能上传PHP文件，因为上传目录是任何人都可以访问的
		    $info = $file->validate(['size'=>156780,'ext'=>'jpg,png,gif,zip,7z'])->move($uploadPath);
		    if($info){
		    	
				$returnArr['code'] = 1;
		       	$data['src'] = str_replace('\\', '/',DS.'uploads'.DS.$info->getSaveName());
				$data['time'] = time();
				$data['name'] = $info->getFilename();
				$data['type'] = $info->getExtension();
				$data['pid'] = $getPid['dirId'];
				//dump($data);
				Db::name('pic_lib')->insert($data);
				$returnArr['data']['src'] = $data['src'];
				
		    }else{
		        // 上传失败获取错误信息
		        $returnArr['code'] = 0;
				$returnArr['msg'] = $file->getError();
		    }
		}else{
			$returnArr['code'] = 0;
			$returnArr['msg'] = '（虚拟）文件夹[ '.input('param.dir').' ]不存在。请先创建';
		}
		return json($returnArr);
	}
	
	
	
	//文件文件夹重命名-2019年1月1日22:54:40
	public function rename() {
		$id=input('param.id');
		$newName=input('param.newName');
		$data['name'] = $newName;
		$r = Db::name('pic_lib')->where('id',input('param.id'))->update($data);
		return json(array('code'=>$r));
	}
	
	//新建文件夹
	public function newDir() {
		$newDirName=input('param.newDirName');
		$data['name'] = $newDirName;
		$data['time'] = time();
		$data['type'] = '/DIR';
		$data['pid'] = input('param.dirId');
		$r=Db::name('pic_lib')->insert($data);
		return json(array('code'=>$r));
	}
	
	//删除文件
	public function delFile() {
		$ids = input('param.ids/a');
		foreach ($ids as $k => $v) {
			$now = Db::name('pic_lib')->where('id',$v)->find();
			if(is_array($now) && $now['type']!='/DIR'){
				if(file_exists(ROOT_PATH.'public'.$now['src'])){
					unlink(ROOT_PATH.'public'.$now['src']);
				}
			}
			$whereDel['pid']=$v;
			$delList = Db::name('pic_lib')->where($whereDel)->select();
			//dump('删除文件(夹)：'.$v);
			$this->loopDir($delList);
			Db::name('pic_lib')->delete($v);
		}
		
		return json(array('code'=>1));
	}
	
	//粘贴
	public function paste(){
		//dump($_GET);
		$type = input('param.type');
		$pathid = input('param.pathid');
		$ids = input('param.ids/a');
		
		if($type==0){
			//剪切
			$update['pid']=$pathid;
			foreach ($ids as $k => $v) {
				Db::name('pic_lib')->where('id',$v)->update($update);
				//$done[]=$v;
			}
		}else{
			//复制
			foreach ($ids as $k => $v) {
				$update = Db::name('pic_lib')->where('id',$v)->find();
				$update['pid']=$pathid;
				if($update['type']!='/DIR'){
					$update['src']=$this->copyFile(ROOT_PATH . 'public' . DS .$update['src']);
				}
				
				$update['time']=time();
				unset($update['id']);
				$pid = Db::name('pic_lib')->insertGetId($update);
				$copyList = Db::name('pic_lib')->where('pid',$v)->select();
				$this->loopDirCopy($copyList,$pid);
				//$done[]=$v;
			}
		}
		return json(array('code'=>1));
	}
	
	//循环文件夹复制
	public function loopDirCopy($copyList,$pid){
		//判断是否为文件夹,为文件夹，继续循环，不为文件夹，保存地址和id
		foreach ($copyList as $k => $v) {
			$update = Db::name('pic_lib')->where('id',$v['id'])->find();
			if($v['type']=='/DIR'){
				$update['pid']=$pid;
				$update['time']=time();
				unset($update['id']);
				$pid = Db::name('pic_lib')->insertGetId($update);
				$copyList_ = Db::name('pic_lib')->where('pid',$v['id'])->select();
				$this->loopDirCopy($copyList_,$pid);
			}else{
				//dump(6);
				$update['pid']=$pid;
				
				$update['src']=$this->copyFile(ROOT_PATH . 'public' . DS .$update['src']);
				$update['time']=time();
				unset($update['id']);
				Db::name('pic_lib')->insertGetId($update);
			}
		}
		
	}
	
	//复制真实路径文件
	public function copyFile($fileName){
		$dir = date("Ymd");
		//dump(5);
		$uploadPath=ROOT_PATH . 'public' . DS . 'uploads' . DS .$dir ;
		if(!file_exists($uploadPath)){
			mkdir($uploadPath,'0777',true);
		}
		$file_type = explode('.', $fileName);
		//dump($file_type);
		if(is_array($file_type)){
			$file_type = $file_type[count($file_type)-1];
		}
		
	
		$newName = md5(time().rand(1,9999)).'.'.$file_type;
		//dump($newName);
		copy($fileName, $uploadPath.DS.$newName);
		return '/uploads/'.$dir.'/'.$newName;
	}
	
	//循环文件夹删除文件
	public function loopDir($delList){
		//判断是否为文件夹,为文件夹，继续循环，不为文件夹，保存地址和id
		foreach ($delList as $k => $v) {
			if($v['type']=='/DIR'){
				//dump('删除文件夹：'.$v['name'].'---id:'.$v['id']);
				$whereDel['pid']=$v['id'];
				$delList_ = Db::name('pic_lib')->where($whereDel)->select();
				//此处填写删除数据库虚拟文件夹
				Db::name('pic_lib')->delete($v['id']);
				$this->loopDir($delList_);
				
			}else{
				//删除数据及文件
				if(file_exists(ROOT_PATH.'public'.$v['src'])){
					//dump('文件存在---');
					unlink(ROOT_PATH.'public'.$v['src']);
				}
				//删除数据表内容
				Db::name('pic_lib')->delete($v['id']);
				//dump($v['id']);
				//dump(ROOT_PATH.'public'.$v['src']);
			}
		}
		
	}
	
	//将所有页面指向此操作，
	public function _empty($name) {
		if(input('param.t')){
			return $this ->$name(input('param.t'));
		}
		
		//以下代码必须建在每个控制器的_empty的操作中
		$func = $this->loadHomeTpl($name);
		if(is_object($func)){
			return $func;
		}else{
			return $this->$func();
		}
		
		
	}
}
?>